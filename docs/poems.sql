CREATE TABLE `poem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(25) NOT NULL DEFAULT '',
  `dynasty` varchar(10) DEFAULT NULL,
  `author` varchar(10) DEFAULT NULL,
  `content` varchar(2000) NOT NULL DEFAULT '',
  `annotation` text,
  `tag` varchar(100) NOT NULL DEFAULT '',
  `relation` varchar(255) NOT NULL DEFAULT '' COMMENT '相关诗词',
  `images` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `poem_author` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(45) DEFAULT '',
  `dynasty` varchar(45) DEFAULT '',
  `introduction` text,
  `magnum_opus` varchar(1000) NOT NULL DEFAULT '',
  `images` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `poem_cipai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45)  NOT NULL DEFAULT '',
  `url` varchar(100)  NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='词牌';