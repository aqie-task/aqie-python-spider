# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import requests


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

def downloadHtml(url):
    response = requests.get(url)

    if response.status_code == 200:
        content = response.text
        # 将网页内容保存到文件
        with open('page.html', 'w', encoding='utf-8') as file:
            file.write(content)
        print('网页下载完成！')
    else:
        print('网页下载失败。')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    downloadHtml('http://www.news.cn/zt/nnzt/2023gnsdxw/index.htm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
