from pymongo import MongoClient

client = MongoClient("mongodb://localhost:27017/")
mdb = client['aqie']
collection = mdb['book_chapter']

filter = {'book_name': 'sony1'}

# mongo 查询数据
res = collection.find_one(filter)
if not res :
    book = {
        'book_name': 'sony1',
        'content':[]
    }
    collection.insert_one(book)
else:
    contents = res['content']
    unit = {"chapter2":"content2"}
    contents.append(unit)

    # 更新数据
    newvalues = {"$set": {'content': contents}}
    collection.update_one(filter, newvalues)
