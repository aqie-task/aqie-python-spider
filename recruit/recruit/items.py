# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
import unicodedata as ucd
from itemloaders.processors import TakeFirst, MapCompose, Join, Compose

def rmEmpty2(s):
    s = ucd.normalize('NFKC', s).replace(' ', '')
    s = s.replace("\r", '')
    s = s.replace("\t", '')
    s = s.replace("\n", '')
    return s

class RecruitItem(scrapy.Item):
    # define the fields for your item here like:
    job_name = scrapy.Field(
        output_processor=TakeFirst()
    )
    company = scrapy.Field(
        output_processor=TakeFirst()
    )
    type = scrapy.Field(
        output_processor=TakeFirst()
    )
    salary = scrapy.Field(
        input_processor=MapCompose(rmEmpty2),
        output_processor=TakeFirst()
    )
    region = scrapy.Field(
        output_processor=TakeFirst()
    )
    education = scrapy.Field(
        output_processor=TakeFirst()
    )
    experience = scrapy.Field(
        output_processor=TakeFirst()
    )
    jd = scrapy.Field()
    jr = scrapy.Field()
    tag = scrapy.Field()
    source = scrapy.Field(
        output_processor=TakeFirst()
    )
    pass
