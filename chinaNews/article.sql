CREATE TABLE `sp_article_detail2` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `url` varchar(100)  NOT NULL DEFAULT '',
  `title` varchar(255)  NOT NULL DEFAULT '',
  `content` text ,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` tinyint NOT NULL DEFAULT '0' COMMENT '类型',
  `images` varchar(2000) NOT NULL DEFAULT '',
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '0-正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;