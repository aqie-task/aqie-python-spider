CREATE TABLE `recruit` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50)  NOT NULL,
  `company` varchar(45)  DEFAULT NULL,
  `type` varchar(45)  DEFAULT NULL COMMENT '公司类型',
  `salary` varchar(45)  NOT NULL,
  `region` varchar(100)  DEFAULT NULL,
  `experience` varchar(45)  DEFAULT NULL,
  `education` varchar(45)  DEFAULT NULL,
  `jd` varchar(2000)  DEFAULT NULL COMMENT '职位描述',
  `jr` varchar(2000)  DEFAULT NULL COMMENT '任职要求',
  `tag` varchar(45)  DEFAULT NULL,
  `source` tinyint NOT NULL DEFAULT '0' COMMENT '来源',
  `url` varchar(45)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;