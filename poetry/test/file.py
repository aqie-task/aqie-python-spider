import unicodedata as ucd

import numpy as np

data = ['a', 'b', 'c']

# 单层列表写入文件
with open("data.txt", "w") as f:
    f.writelines(data)

msg = '中 国          q  1'
msg2 = '中　国   q'
# print([hex(ord(s)) for s in msg])
# print([hex(ord(s)) for s in msg2])

# print(''.join(msg.split()))
# print(''.join(msg2.split()))
# print(ucd.normalize('NFKC', msg).replace('　', ''))
# print(ucd.normalize('NFKC', msg).replace(' ', ''))


def handle_addr(s):
    s = s.replace("\r", '')
    s = s.replace("\t", '')
    s = s.replace("\n", '')
    if s.strip() != '':
        return s
    else:
        return ''

def data_empty():
    lst = ['\n                \n', '            1']
    up_lst = map(lambda x: handle_addr(x), lst)
    # odd_lst = filter(lambda x: x != '', up_lst)
    # 解析器
    odd_lst = [e for e in up_lst if e != '']
    print(odd_lst)

    a = np.array(odd_lst)
    # a.all() > 0:
    if odd_lst:
        print('list is not empty')
        return False
    else:
        print('list is empty')
        return True


print(data_empty())
