import bson
import pymongo

client = pymongo.MongoClient(host="127.0.0.1", port=27017)

mydb = client["aqie"]
# 存放数据的数据库表名
sheet = mydb["doubanmovies"]
# Expected type 'Union[MutableMapping[str, Any], RawBSONDocument]', got 'Set[str]' instead
sheet.insert_one({'a': 'b'})