""" 用户id 商品id 评分 时间戳， 评分1-5 用户id 1-2000 商品id 1-10000 ，python 随机生成 40000条数据，保存到ratinbg.csv 

4867,457976,5.0,1395676800


"""
import csv
import random
import time

# 生成随机评分数据
def generate_rating_data(num_records):
    with open('rating.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        # 写入CSV文件头部
        writer.writerow(['用户id', '商品id', '评分', '时间戳'])
        # 生成随机评分数据并写入文件
        for _ in range(num_records):
            user_id = random.randint(1, 2000)
            product_id = random.randint(1, 10000)
            rating = round(random.uniform(1, 5), 1)  # 随机评分在1到5之间
            timestamp = int(time.time()) - random.randint(0, 365 * 24 * 60 * 60)  # 随机生成一个一年内的时间戳
            writer.writerow([user_id, product_id, rating, timestamp])

# 生成40000条随机评分数据并保存到CSV文件中
generate_rating_data(40000)
