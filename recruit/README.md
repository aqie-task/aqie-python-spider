#### doc
- [Python爬虫实例——scrapy框架爬取拉勾网招聘信息](https://cloud.tencent.com/developer/article/1724352)

```
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple scrapy-fake-useragent
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple cryptography==36.0.2
```

```zhilian
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://sou.zhaopin.com/?jl=575

job_unit=response.xpath('//*[@class="positionlist"]/div[contains(@class,"joblist-box__item")]')
for r in job_unit:
    print(r.xpath('./a/div[1]/div/span/text()').extract_first())    # 岗位名
    print(r.xpath('./a/div[1]/div[2]/span/text()').extract_first()) # 企业名
    print(r.xpath('./a/div[2]/div/span/text()').extract_first())    # 性质
    print(r.xpath('./a/div[2]/div/p/text()').extract_first())    # 薪水
    print(r.xpath('./a/div[2]/div/ul').xpath('string(.)').extract())    # region
```

```
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.lagou.com/wn/jobs?kd=Java&city=%E5%8C%97%E4%BA%AC
```

```lagou
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.lagou.com/beijing-zhaopin/Java/
https://www.lagou.com/beijing-zhaopin/Java/2/?filterOption=2&sid=18c27979d35048da9d7d6f48d12e769e

url=response.xpath('//*[@id="s_position_list"]//div[@class="list_item_top"]/div/div/a/@href').extract()


scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.lagou.com/wn/jobs/10947575.html?show=18c27979d35048da9d7d6f48d12e769e
job_name=response.xpath('//div[@class="position-content-l"]')
job_name=response.xpath('//div[@class="position-content-l"]//div[@class="position-head-wrap-position-name"]/text()').extract_first()
company
type
salary
region
experience
education
jd
jr
url
```

```liepin
scrapy genspider liepin https://www.liepin.com/zhaopin
scrapy shell https://www.liepin.com/zhaopin/?inputFrom=www_index&workYearCode=0&key=java&scene=hot_search&ckId=ac5siech9s8beyjaluo0z239c0hphz88&dq=

```