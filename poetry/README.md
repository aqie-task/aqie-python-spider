- [category](https://www.shicimingju.com/shicimark)
  - 图片
  - https://www.shicimingju.com/chaxun/list/4553.html
  - 其他作品

- [rank](https://www.shicimingju.com/paiming)
  - 分页
- [book](https://www.shicimingju.com/cate?cate_id=4)
  - https://www.shicimingju.com/cate?cate_id=18
  - https://www.shicimingju.com/chaxun/list/5651.html

- [textbook](https://www.shicimingju.com/hecheng/index.html)

```
拿取嵌套节点的内容时候，使用string(.)方法效果更好
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple pillow
```

```cipai
scrapy genspider cipai https://www.shicimingju.com/cipai/index.html
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/cipai/index.html 
title = response.xpath('//*[@id="main_left"]//ul/li/a/text()') 
url=response.xpath('//*[@id="main_left"]//ul/li/a/@href')   
```

```shici url
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/chaxun/shici/%E5%85%A5%E5%A1%9E

url = response.xpath('//*[@id="main_left"]//div[@class="shici_list_main"]/h3/a/@href')
url.extract()

```

```shici detail
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/chaxun/list/3710.html

title=response.xpath('//*[@id="zs_title"]').xpath('string(.)')

tag=response.xpath('//*[@class="shici-mark"]/a/text()').extract()
relation=response.xpath('//*[@class="shici_list_main"]/h3/a/text()').extract()

images=response.xpath('//*[@id="item_div"]/img/@src').extract_first()
title.extract()
tag
relation
images

dynasty=response.xpath('//div[@class="niandai_zuozhe"]/text()')
dynasty.extract()

author=response.xpath('//div[@class="niandai_zuozhe"]/a/text()')
author.extract()

content=response.xpath('//*[@id="zs_content"]').xpath('string(.)')
content.extract()

annotation=response.xpath('//*[@id="item_shangxi"]').xpath('string(.)')
annotation.extract()
```

``` author list
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/category/all
author=response.xpath('//*[@id="main_left"]/div/div[@class="zuozhe_list_item"]/h3/a/text()')
author_url=response.xpath('//*[@id="main_left"]/div/div[@class="zuozhe_list_item"]/h3/a/@href')
author.extract()
author_url.extract()
next_url=response.xpath('//*[@id="list_nav_part"]/a[contains(text(),"下一页")]/@href').extract_first()

// 作者诗词列表
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/chaxun/zuozhe/91.html
poem_urls=response.xpath('//*[@id="main_left"]//div[@class="shici_list_main"]/h3/a/@href').extract()
poem_urls

next_url=response.xpath('//*[@id="list_nav_part"]/a[contains(text(),"下一页")]/@href').extract_first()
next_url

next_url=response.xpath('//*[@id="list_nav_part"]/a[last()-1]/@href')
```

``` do author
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/category/all
author_unit = response.xpath('//*[contains(@class,"zuozhe_card")]')
for r in author_unit:
  print(r.xpath('./div[@class="zuozhe_list_item"]/h3/a/text()').extract_first())
  print(r.xpath('./div[@class="zuozhe_good_shici_div"]/a/text()').extract())
  print(r.xpath('./div[@class="zuozhe_list_item"]/h3/a/@href').extract_first())
  print(r.xpath('./div[@class="zuozhe_list_item"]/div[@class="zuozhe_list_des"]/img/@src').extract_first())
author

magnum_opus

scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/chaxun/zuozhe/1.html
author = response.xpath('//*[@id="main_right"]/div[contains(@class,"about_zuozhe")]/div/div/h4/a/text()').extract_first()
author
dynasty = response.xpath('//*[@id="main_right"]/div[contains(@class,"about_zuozhe")]/div/div[@class="aside_left"]/div/a/text()').extract_first()
dynasty
introduction = response.xpath('//*[@id="main_right"]/div[contains(@class,"about_zuozhe")]/div/div/div[@class="des"]').xpath('string(.)').extract()
introduction
```

```book
# book
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/book/
book_name = response.xpath('//*[@id="main_left"]/div[contains(@class,"booknark_card")]/ul/li/a/text()').extract()
urls = response.xpath('//*[@id="main_left"]/div[contains(@class,"booknark_card")]/ul/li/a/@href').extract()

# chapter
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/book/sanguoyanyi.html
book_name=response.xpath('//*[@id="main_left"]/div[contains(@class,"bookmark-list")]/h1/text()').extract()
chapter=response.xpath('//*[@class="book-mulu"]/ul/li/a/text()').extract()
url=response.xpath('//*[@class="book-mulu"]/ul/li/a/@href').extract()

# book
scrapy shell -s USER_AGENT="Mozilla/5.0 (Windows NT 10.0; …) Gecko/20100101 Firefox/60.0" https://www.shicimingju.com/book/sanguoyanyi/31.html
content=response.xpath('//*[@id="main_left"]//div[@class="chapter_content"]/text()')
content=response.xpath('//*[@id="main_left"]//div[@class="chapter_content"]/p/text()')
content.extract()
```

```
https://www.shicimingju.com/shicimark
https://www.shicimingju.com/shicimark/tianyuanshi.html
https://www.shicimingju.com/chaxun/list/4553.html
```
