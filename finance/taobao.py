import time
import re
from selenium import webdriver
import pandas as pd

# 1.访问网址并手动模拟登录
browser = webdriver.Chrome()
url = 'https://runbaiyan.tmall.com/search.htm?spm=a1z10.1-b-s.w5001-21884551106.3.3f0d7f68FOYkqz&search=y&scene=taobao_shop'
browser.get(url)

# 2.获取网页源代码
data = browser.page_source

# 3.正则表达式提取相关信息
p_sales = '<span class="sale-num">(.*?)</span>'
sales = re.findall(p_sales, data, re.S)  # 偶尔有时候会有换行，所以得加上re.S忽略换行的影响
p_price = '<span class="c-price">(.*?)</span>'
price = re.findall(p_price, data, re.S)
p_name = '<a class="item-name J_TGoldData".*?>(.*?)</a>'  # 2021版：还是定位标题那块的网页源代码编写正则好一些
name = re.findall(p_name, data, re.S)

# 4.数据清洗及打印
for i in range(len(name)):
    name[i] = name[i].strip()  # 清除有的标题两旁的空格
    price[i] = price[i].strip()  # 清除价格两旁的空格
    print('商品名称为：' + name[i])
    print('商品价格为：' + price[i])
    print('商品销量为：' + sales[i])

# 5.数据导出到Excel:写法2（个人更喜欢一些）:通过列表构造法
file_name = time.strftime('%Y-%m-%d') + '润百颜销量情况_2.xlsx'
print(file_name)  # 打印下构造的文件名

df = pd.DataFrame()
df['名称'] = name
df['销量'] = sales
df['价格'] = price

df.to_excel(file_name, index=False)