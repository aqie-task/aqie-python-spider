import scrapy
from scrapy_splash import SplashRequest

class ChinanewsSpider(scrapy.Spider):
    name = 'chinanews'
    allowed_domains = ['www.chinanews.com.cn']
    start_urls = ['https://channel.chinanews.com.cn/cns/cl/cj-fortune.shtml']

    def start_requests(self):
        yield SplashRequest('https://channel.chinanews.com.cn/cns/cl/cj-fortune.shtml',
                            args={'wait': 0.5})
    def parse(self, response):
        print(response.text)
        with open("cj-fortune.shtml", "w") as text_file:
            text_file.write(response.text)
        pass
