# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import codecs
import json
import pymongo
from scrapy.utils.project import get_project_settings

class DoubanmoviePipeline:
    settings = get_project_settings()
    host = settings["MONGODB_HOST"]
    port = settings["MONGODB_PORT"]
    dbname = settings["MONGODB_DBNAME"]
    sheetname = settings["MONGODB_SHEETNAME"]

    # 创建MONGODB数据库链接
    client = pymongo.MongoClient(host=host, port=port)
    # 指定数据库
    mydb = client[dbname]
    # 存放数据的数据库表名
    sheet = mydb[sheetname]
    def process_item(self, item, spider):
        # 1. 生成文件
        self.filename = codecs.open('movie.json','a',encoding='utf-8')
        html = json.dumps(dict(item),ensure_ascii=False)
        self.filename.write(html + '\n')
        self.filename.close()
        # 2. 把数据插入数据库
        data = dict(item)
        print("data", data)
        self.sheet.insert_many(data)

        return item
