# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from itemloaders.processors import TakeFirst, MapCompose, Join, Compose
import unicodedata as ucd

def rmEmpty2(s):
    s = ucd.normalize('NFKC', s).replace(' ', '')
    s = s.replace("\r", '')
    s = s.replace("\t", '')
    s = s.replace("\n", '')
    return s

class BlogsItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    content = scrapy.Field()
    tag = scrapy.Field(
        input_processor=MapCompose(rmEmpty2),
        output_processor=Join(',')
    )
    view_num = scrapy.Field()
    type = scrapy.Field()
    pass
