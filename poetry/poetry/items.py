# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
from urllib.parse import unquote

import scrapy
import unicodedata as ucd
from itemloaders.processors import TakeFirst, MapCompose, Join, Compose
from scrapy.loader import ItemLoader


def strip(s):
    return s.strip()

def rmEmpty(s):
    s = ucd.normalize('NFKC', s).replace('　', '')
    #s = ucd.normalize('NFKC', s).replace(' ', '')
    s = s.replace("\r", '')
    s = s.replace("\t", '')
    s = s.replace("\n", '')
    # s = ''.join(s.split())
    return s

def rmEmpty2(s):
    s = ucd.normalize('NFKC', s).replace(' ', '')
    s = s.replace("\r", '')
    s = s.replace("\t", '')
    s = s.replace("\n", '')
    return s

class ArticleItemLoader(ItemLoader):
    default_input_processor = TakeFirst()
    pass

class ArticleItemLoader2(ItemLoader):
    default_output_processor = TakeFirst()
    pass


class PoetryItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    dynasty = scrapy.Field()
    author = scrapy.Field()
    content = scrapy.Field(
        output_processor=MapCompose(strip)
    )
    annotation = scrapy.Field(
        output_processor=MapCompose(strip)
    )
    tag = scrapy.Field(
        output_processor=Join(',')
    )
    relation = scrapy.Field(
        output_processor=Join(',')
    )
    images = scrapy.Field()
    image_path = scrapy.Field()
    pass


class CipaiItem(scrapy.Item):
    title = scrapy.Field()
    url = scrapy.Field(
        input_processor=MapCompose(unquote)
    )
    pass

class AuthorItem(scrapy.Item):
    url = scrapy.Field(
        output_processor=TakeFirst()
    )
    author = scrapy.Field(
        output_processor=TakeFirst()
    )
    dynasty = scrapy.Field(
        output_processor=TakeFirst()
    )
    introduction = scrapy.Field(
        input_processor=MapCompose(rmEmpty2),
        output_processor=TakeFirst()
    )
    magnum_opus = scrapy.Field(
        output_processor=Join(',')
    )
    images = scrapy.Field()
    image_path = scrapy.Field()
    pass



class BookCateItem(scrapy.Item):
    cate = scrapy.Field()
    description = scrapy.Field()
    pass

class BookItem(scrapy.Item):
    book_name = scrapy.Field(
        output_processor=TakeFirst()
    )
    chapters = scrapy.Field()
    url = scrapy.Field()
    pass

class BookContent(scrapy.Item):
    book_name = scrapy.Field(
        input_processor=MapCompose(strip),
        output_processor=TakeFirst()
    )
    chapter = scrapy.Field(
        input_processor=MapCompose(strip),
        output_processor=TakeFirst()
    )
    content = scrapy.Field(
        input_processor=MapCompose(rmEmpty),
    )
    pass